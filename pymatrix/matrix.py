from typing import List, Union
from operator import add as _add, mul as _mul, truediv as _div

Number = Union[int, float]


class Matrix:
    """
    """

    def __init__(self, data: List[List[Number]]):
        if not isinstance(data, list):
            raise AttributeError("A matrix must be initiated with a list")

        if not isinstance(data[0], (int, float, List)):
            raise AttributeError("A matrix must contain only vectors or numbers")

        if isinstance(data[0], List):
            if not isinstance(data[0][0], (int, float)):
                raise AttributeError(
                    "The inner vector of a matrix should only contain numbers"
                )

        self.data = data

    def __repr__(self):
        return f"Matrix({str(self.data)})"

    def __getitem__(self, key):
        tmp = self.data[key]
        if isinstance(tmp, int):
            return [tmp]
        return tmp

    def __iter__(self):
        for row in self.data:
            yield row

    @property
    def shape(self):
        nrows = len(self.data)
        if isinstance(self.data[0], List):
            ncols = len(self.data[0])
        else:
            ncols = 1

        return (nrows, ncols)

    @property
    def is_square(self):
        if self.shape[0] == self.shape[1]:
            return True
        return False

    @classmethod
    def zeros(cls, shape):
        return cls([[0 for _ in range(shape[1])] for _ in range(shape[0])])

    @classmethod
    def _element_wise(cls, operator):
        def wrapper(self, other):
            result = list(Matrix.zeros(self.shape))
            if isinstance(other, (int, float)):
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        result[i][j] = operator(self[i][j], other)

            elif isinstance(other, Matrix):
                if self.shape != other.shape:
                    raise AttributeError(
                        "Matrices can only be added if they have the same shape"
                    )
                for i in range(self.shape[0]):
                    for j in range(self.shape[1]):
                        result[i][j] = operator(self[i][j], other[i][j])

            elif isinstance(other, list):
                return operator(self, Matrix(other))

            else:
                raise AttributeError(
                    f"Matrix object cannot be added to {type(other)} type "
                )
            return cls(result)

        return wrapper

    def __add__(self, other: Union["Matrix", Number]):
        """
        >>> m = Matrix([1, 2])
        >>> m + 1
        Matrix([[2], [3]])
        >>> m + m
        Matrix([[2], [4]])
        >>> m + [1, 2]
        Matrix([[2], [4]])

        >>> m + [1, 2, 3]
        Traceback (most recent call last):
            ...
        AttributeError: Matrices can only be added if they have the same shape
        """
        add = Matrix._element_wise(_add)
        return add(self, other)

    def __mul__(self, other):
        """
        """
        
        mul = Matrix._element_wise(_mul)
        return mul(self, other)

    def __truediv__(self, other):
        div = Matrix._element_wise(_div)
        return div(self, other)

    def transpose(self):
        return Matrix(
            [[self[i][j] for i in range(self.shape[0])] for j in range(self.shape[1])]
        )

    @property
    def T(self):
        return self.transpose()

    def __matmul__(self, other):
        assert self.shape[1] == other.shape[0]
        C = list(Matrix.zeros((self.shape[0], other.shape[1])))
        for i in range(self.shape[0]):
            for j in range(other.shape[1]):
                for k in range(self.shape[1]):
                    C[i][j] += self[i][k] * other[k][j]

        return Matrix(C)


if __name__ == "__main__":
    import doctest

    doctest.testmod()
