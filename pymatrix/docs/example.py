from pymatrix.matrix import Matrix

m = Matrix([1, 2])
n = Matrix([
    [1, 2],
    [3, 4]
])

p = Matrix([
    [5, 6],
    [7, 8]
])

p + m
n + n
n @ p
n * p 